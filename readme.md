# OpenLayers / Vector Layer

Un uso común de _*OL*_ es la creación y manipulación de capas vectoriales para representar e interactuar con geometrias, como puntos, líneas y polígonos. En este ejemplo se muestra como visualizar e interactuar con las ecoregiones de los países utilizando capas vectoriales en _*OL*_.

Para iniciar, se crea una capa vectorial utilizando la clase ``o/layer/Vector`, la cual requiere una fuente que pueda leer formatos vectoriales como: `ol.source.GeoJSON`, `ol.source.KML` o `ol.source.TopoJSON`. En este caso se utilizara un archivo[GeoJSON] (https://openlayers.org/data/vector/ecoregions.json) para cargar las ecoregiones de los paises, permitiendo que la información correspondiente se muestre al pasar el cursor y hacer click sobre ellas.

El siguiente fragmento de código muestra como crear esta capa vectorial:

```js
const vectorLayer = new VectorLayer({
  background: '#1a2b39',
  source: new VectorSource({
    url: 'https://openlayers.org/data/vector/ecoregions.json',
    format: new GeoJSON(),
  }),
  style: {
    'fill-color': ['string', ['get', 'COLOR'], '#eee'],
  },
});
```

Una vez creada la capa vectorial, el siguiente paso es inicializar el mapa y cargar la capa vectorial creada previamente. Este proceso define el contenedor del mapa, el punto de vista inicial(centro y nivel de zoom), y las capas que incluira el mapa.

```js
const map = new Map({
  layers: [vectorLayer],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 1,
  }),
});
```
Despúes se agregan caracteristicas que resalten las ecoregiones al pasar el cursosr sobre ellasen el mapa. Esto se logra creando un _overlay_ de capa vectorial.

```js
const featureOverlay = new VectorLayer({
  source: new VectorSource(),
  map: map,
  style: {
    'stroke-color': 'rgba(255, 255, 255, 0.7)',
    'stroke-width': 2,
  },
});

```
Ahora se crearan los elementos HTML para poder visualizarlos en el mapa.

```js
let highlight;
const displayFeatureInfo = function (pixel) {
  const feature = map.forEachFeatureAtPixel(pixel, function (feature) {
    return feature;
  });
```

>El método `forEachFeatureAtPixel`en una función que es utilizada para interactuar con las caracteristicas(feature) de un píxel en la ventana gráfica y ejecuta una devolución de llamada con cada entidad que se cruza. Esta función es útil en la interacción del usuario con el mapa, como cuando se quiere mostrar información sobre un elemento al pasar el cursor sobre él al hacer clic.

```js
  const info = document.getElementById('info');
  if (feature) {
    info.innerHTML = feature.get('ECO_NAME') || '&nbsp;';
  } else {
    info.innerHTML = '&nbsp;';
  }

  if (feature !== highlight) {
    if (highlight) {
      featureOverlay.getSource().removeFeature(highlight);
    }
    if (feature) {
      featureOverlay.getSource().addFeature(feature);
    }
    highlight = feature;
  }
};
```

Finalmente se agregaran eventos al mapa que responadan a las acciones del usuario, como mover el cursor para mostrar la información sobre las ecoregiones. Estos eventos permiten que la aplicación responda dinámicamente a las interaciones del usiario , mejorando la experiencia de explorar el mapa.

```js
map.on('pointermove', function (evt) {
  if (evt.dragging) {
    return;
  }
  const pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel);
});

map.on('click', function (evt) {
  displayFeatureInfo(evt.pixel);
});
```



